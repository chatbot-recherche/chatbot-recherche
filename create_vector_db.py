from langchain import PromptTemplate, LLMChain
from langchain.llms import GPT4All
from langchain.callbacks.base import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain.document_loaders import TextLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.document_loaders.sitemap import SitemapLoader
from langchain.embeddings import LlamaCppEmbeddings
from langchain.vectorstores.faiss import FAISS
import os 
import datetime
from tqdm import tqdm

# Path to GPT4All model and Alpaca embeddings 
gpt4all_path = './models/gpt4all-converted.bin' 
llama_path = './models/ggml-model-q4_0.bin' 
# Calback manager for handling the calls with  the model
callback_manager = CallbackManager([StreamingStdOutCallbackHandler()])

# create the embedding object
embeddings = LlamaCppEmbeddings(model_path=llama_path)
# create the GPT4All llm object
llm = GPT4All(model=gpt4all_path, callback_manager=callback_manager, verbose=True)

# Split text 
def split_chunks(sources):
    chunks = []
    splitter = RecursiveCharacterTextSplitter(chunk_size=516, chunk_overlap=64)
    for chunk in tqdm(splitter.split_documents(sources)):
        chunks.append(chunk)
    return chunks


def create_index(chunks):
    texts = [doc.page_content for doc in chunks]
    metadatas = [doc.metadata for doc in chunks]

    search_index = FAISS.from_texts(texts, embeddings, metadatas=metadatas)

    return search_index

#===========SITEMAP LOADER======================

general_start = datetime.datetime.now() 

# Only urls that match one of the filtered patterns will be loaded
loader = SitemapLoader(
    "https://doranum.fr/wp-sitemap.xml",
    filter_urls=["https://doranum.fr/metadonnees-standards-formats/", 
                 "https://doranum.fr/glossaire-donnees-recherche/", 
                 "https://doranum.fr/stockage-archivage/"]
)
docs = loader.load()
print("Generating vector database....")
# Split the documents in chunks
chunks = split_chunks(docs)
# Create the vector database
db0 = create_index(chunks)
print("Saving Merged Database Locally")
# Save the databasae locally
db0.save_local("faiss_index_doranum_sample")

general_end = datetime.datetime.now() 
general_elapsed = general_end - general_start 
print(f"All indexing completed in {general_elapsed}")

#==========END SITEMAP LOADER===============




