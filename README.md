# ChatBot Données de la Recherche (TODO:nom à trouver)

## Présentation

Concept : la question de l'utilisateur est envoyée à un LLM, avec en contexte un cadre de réponse ainsi que le contenu des documents les plus similaires parmi une base de données de référence.

## Workflow

#### Entrainement (exécuté 1 fois)

Les documents de référence - comme le contenu d'un site web - sont divisés en plus petits *chunks* puis transformés en *embeddings* puis stockés dans une *vector database* et indexés.  


#### Obtention d'une réponse (exécuté à chaque demande)

Grâce à l'index, une recherche par similarité est effectuée entre la question de l'utilisateur et la vector database. Les résultats les plus proches sont utilisés comme contexte à la question, et tous deux sont soumis au LLM pour en tirer une réponse.


## Installation

#### Télécharger les modèles

Le LLM qui répondra aux questions doit être téléchargé puis placé dans le dossier `.\models\`. Pour un premier essai, vous pouvez par exemple choisir GPT4All qui a l'avantage d'être libre et de fonctionner sur CPU, disponible en téléchargement [ici](https://huggingface.co/mrgaang/aira/blob/main/gpt4all-converted.bin). D'autres modèles sont disponibles sur [Hugging Face](https://huggingface.co/).

Le modèle d'embedding (fichier `.bin`) pour représenter les documents doit également être placé dans le dossier `.\models\`. Différents modèles sont aussi disponibles sur [Hugging Face](https://huggingface.co/), parmi eux l'embedding LLamaCPP de ce modèle [Alpaca](https://huggingface.co/Pi3141/alpaca-native-7B-ggml/tree/397e872bf4c83f4c642317a5bf65ce84a105786e) est pratique car assez petit.

#### Dépendences

Toutes les dépendences sont répertoriées dans le `requirements.txt`. Pour les installer, il faut simplement utiliser la commande `pip install -r requirements.txt` dans son propre [environnement virtuel](https://www.freecodecamp.org/news/how-to-setup-virtual-environments-in-python/), avec Python 3.11. Attention, llama-cpp-python requiert d'avoir déjà [CMake](https://cmake.org/install/) d'installé.

## Utilisation

#### Création de la Vector Database

Une Vector Database a déjà été créée, basée sur un échantillon du site web [doranum](https://doranum.fr/). Le contenu utilisé est celui de toutes les pages qui débutent par l'une de ces trois url : ` https://doranum.fr/metadonnees-standards-formats/`, ` https://doranum.fr/glossaire-donnees-recherche/`, ou ` https://doranum.fr/stockage-archivage/`. Elle est rangée ici : `.\index\faiss_index_doranum_sample\`.

Elle est en revanche très incomplète et petite, ce qui conduit à beaucoup d'erreurs dans les réponses du chatbot final. Pour créer sa propre Vector Database à partir de ses propres urls, cela se passe dans le fichier `create_vector_db.py`. Il est aussi possible d'ajouter d'autres types de [loaders](https://python.langchain.com/docs/modules/data_connection/document_loaders/) qui prennent en compte d'autre types de documents.

Une fois le fichier prêt, il suffit de le lancer avec la commande : `python create_vector_db.py`

#### Interrogation du ChatBot

Pour le moment, la question à poser au chatbot est hardcodée dans le fichier `answer_users_question.py`. Et pour obtenir la réponse du chatbot, il faut simplement lancer le script :   `python answer_users_question.py`


## A faire

- trouver de bons moyens d'évaluation du chatbot
- meilleure similarity search, qui passe peut-être par meilleur choix d'indexation/de vecteur Db
- récolter les données du site autre que le simple texte de la page (pdfs, slides, vidéos)
- trouver meilleurs modèles, plus adaptés (suggestion de Max : Falcon, maintenant disponible sur GPT4All)
- trouver les ressources pour faire tourner ces meilleurs modèles
- tester différentes chunk sizes pour découper les documents

